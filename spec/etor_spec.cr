require "./spec_helper"

describe Ternary do
  describe Ternary::Tryte do
    it "retrieves original integer" do
      {0_i16, 7_i16, -12_i16, 364_i16}.each do |i|
        t = Ternary::Tryte.new i
        t.val.should eq i
      end
    end

    it "converts trit array" do
      {
        {[0_i8, 0_i8, 0_i8, 0_i8, 0_i8, 1_i8], 1_i16},
        {[0_i8, 0_i8, 1_i8, 0_i8, -1_i8, -1_i8], 23_i16},
        {[-1_i8, 1_i8], -2_i16},
      }.each do |i|
        t = Ternary::Tryte.new i[0]
        t.val.should eq i[1]
      end
    end
  end
end
