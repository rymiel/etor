require "./ternary"
require "../etor"
require "log"

macro cpu3(reg_width, segm = false)
  REG_WIDTH = {{reg_width}}
  alias Tr = Ternary::Tryte
  alias Addr = Ternary::TNum(REG_WIDTH)

  module Mem
    class Wrapper
      Log = ::Log.for(self)
      property position : Addr = Addr.new
      @hash : Hash(Addr, Tr)

      def initialize(@s : Sim)
        @hash = @s.mem
      end

      def read({% if segm %}*, x : Bool = false{% end %})
        {% if segm %}
        @s.assert_segment(@position, Sim::Permission::Readable)
        @s.assert_segment(@position, Sim::Permission::Executable) if x
        @s.negative_assert_segment(@position, Sim::Permission::Executable) unless x
        @s.negative_assert_segment(@position, Sim::Permission::Writable) if x
        {% end %}
        ret = @hash[@position]
        Log.trace &.emit "mR{% if segm %}#{x ? 'x' : ' '}{% end %} :: #{@position} -> #{ret}"
        seek
        ret
      end

      def write(t : Tr{% if segm %}, *, x : Bool = false{% end %})
        @hash[@position] = t
        Log.trace &.emit "mW :: #{@position} <- #{t} #{t.trit_s}"
        seek
        nil
      end

      def read(i : Int{% if segm %}, *, x : Bool = false{% end %})
        Array(Tr).new(i) { read {% if segm %} x: x {% end %} }
      end

      def write(a : Iterable(Tr){% if segm %}, *, x : Bool = false{% end %})
        a.each { |i| write i, {% if segm %} x: x {% end %} }
      end

      def seek(i = 1)
        @position = @position.succ(i)
      end

      def from(i : Addr, &)
        old_pos = @position
        Log.debug &.indented("Jumping to #{i}")
        @position = i
        a = yield
        Log.debug &.end("Jumping back to #{old_pos}")
        @position = old_pos
        a
      end
    end

    class Reader
      getter stream : Wrapper
      @pos = 0
      @buffer = Tr.new 0
      Log = ::Log.for(self)

      def initialize(@stream)
      end

      def trits(amount{% if segm %}, *, x : Bool = false{% end %})
        if amount + @pos > 6_u8
          raise IndexError.new("Must finish reading remaining #{6 - @pos} trits before reading the next #{amount}")
        end
        if @pos == 0
          @buffer = @stream.read{% if segm %} x: x{% end %}
        end
        trit_array = @buffer.trit_a
        upper = @pos
        lower = @pos + amount
        actual = Tr.new trit_array[upper...lower]
        Log.debug &.emit "#{@buffer} [#{upper}...#{lower}] -> #{actual} (#{actual.trit_s})"
        @pos += amount
        @pos %= 6
        actual
      end

      def tryte{% if segm %}(*, x : Bool = false){% end %}
        trits 6{% if segm %}, x: x{% end %}
      end

      def tribble{% if segm %}(*, x : Bool = false){% end %}
        trits 3{% if segm %}, x: x{% end %}
      end

      def read(i : Addr, amt : Int{% if segm %}, *, x : Bool = false{% end %})
        @stream.from i do
          @stream.read amt{% if segm %}, x: x{% end %}
        end
      end

      def write(i : Addr, data : Iterable(Tr){% if segm %}, *, x : Bool = false{% end %})
        @stream.from i do
          @stream.write data{% if segm %}, x: x{% end %}
        end
      end

      def repeated_write(i : Addr, data : Iterable(Tr), times : Int32{% if segm %}, *, x : Bool = false{% end %})
        @stream.from i do
          times.times do
            @stream.write data{% if segm %}, x: x{% end %}
          end
        end
      end

      def write(i : Addr, data : String{% if segm %}, *, x : Bool = false{% end %})
        write(i, data.gsub(/\s+/, "").scan(/.{1,2}/).map { |j| Tr.new j[0] }{% if segm %}, x: x{% end %})
      end
    end
  end

  class Sim
    {% if segm %}
    @[Flags]
    enum Permission
      Readable
      Writable
      Executable

      def self.rx
        Readable | Executable
      end

      def self.r
        Readable
      end
    end

    getter segments : Array(Segment) = [] of Segment
    alias Segment = {Range(Addr, Addr), Permission}
    {% end %}

    getter reg = Reg.new
    getter mem = Hash(Addr, Tr).new(Tr.new(0))
    getter! memread
    Log = ::Log.for(self)

    def initialize
      @memread = Mem::Reader.new(Mem::Wrapper.new(self))
    end

    def hypervisor_execute(from)
      log = ::Log.for("HYPERVISOR")
      begin
        execute_at(from)
      rescue violation : Violation::ViolationException
        log.dedent_all
        log.fatal &.emit (violation.message || "Unknown violation")
        log.fatal &.emit violation.diagnostic
      rescue ex : Exception
        log.dedent_all
        traceback = ex.backtrace?
        log.error &.emit "Uncaught #{ex.class}: #{ex.message}"
        if traceback
          log.will_indent
          traceback.each do |frame|
            log.error &.emit frame # if frame.starts_with? "src"
          end
          log.error &.end
        end
      end
    end

    {% if segm %}
    def containing_segment(addr : Addr)
      match = @segments.select { |i|
        i[0].includes? addr
      }
      raise IndexError.new if match.size > 1
      if match.size == 1
        match.first[1]
      else
        Permission::None
      end
    end

    def assert_segment(addr : Addr, permission : Permission)
      raise Violation::SegmentationFault.new addr, permission unless containing_segment(addr).includes? permission
    end

    def negative_assert_segment(addr : Addr, permission : Permission)
      raise Violation::SegmentationFault.new addr, permission, true if containing_segment(addr).includes? permission
    end
    {% end %}
  end

  module Violation
    class ViolationException < Exception
      getter diagnostic : String = "No additional diagnostic info"
      @message = "Unknown fatal violation"

      def initialize(message)
        @message = "Fatal violation: " + message
      end
    end

    {% if segm %}
    class SegmentationFault < ViolationException
      def initialize(location : Addr, attempted : Sim::Permission?, negative = false)
        super("Segmentation fault")
        @diagnostic = "Memory location #{location}"
        @diagnostic += " lacks #{"negative " if negative}permission for #{attempted}" if attempted
      end
    end
    {% end %}
  end

  module TernaryAscii
    extend self

    def convert(a)
      String.build do |str|
        a.each do |t|
          str <<
            case t
            when "+-+-+-" then '@'
            when "000000" then "\x00"
            else
              trit = t.trit_s
              is_second = trit.includes? '+'
              bits = (is_second ? "1" : "0") + trit.gsub({'+' => '1', '-' => '1'})
              bits.to_i(2).chr
            end
        end
      end
    end
  end

  class ::String
    def ~ : {{@type.name}}::Addr
      {{@type.name}}::Addr.new(self.split(" "))
    end
  end
end
