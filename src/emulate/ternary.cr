require "big"

class String
  def ===(other : Ternary::Tryte)
    other == Ternary::Tryte.new(self)
  end
end

module Ternary
  TRIBBLE_MAX = (1 + 3 + 3 ** 2)
  TRIBBLE_MIN = -TRIBBLE_MAX
  TRYTE_MAX   = TRIBBLE_MAX + TRIBBLE_MAX * 3 ** 3
  TRYTE_MIN   = -TRYTE_MAX
  TRYTE_RANGE = TRYTE_MIN..TRYTE_MAX

  TRIBBLES = {
    "---" => 'm',
    "--0" => 'l',
    "--+" => 'k',
    "-0-" => 'j',
    "-00" => 'i',
    "-0+" => 'h',
    "-+-" => 'g',
    "-+0" => 'f',
    "-++" => 'e',
    "0--" => 'd',
    "0-0" => 'c',
    "0-+" => 'b',
    "00-" => 'a',
    "000" => '0',
    "00+" => 'A',
    "0+-" => 'B',
    "0+0" => 'C',
    "0++" => 'D',
    "+--" => 'E',
    "+-0" => 'F',
    "+-+" => 'G',
    "+0-" => 'H',
    "+00" => 'I',
    "+0+" => 'J',
    "++-" => 'K',
    "++0" => 'L',
    "+++" => 'M',
  }

  TM = -1_i8
  TZ =  0_i8
  TP =  1_i8

  module BalancedTritNotation
    extend self

    def to_a(i : Int)
      sign = i.sign
      i = i.abs
      ret = Array(Int8).new 6
      until i == 0
        case i % 3
        when 0
          ret << TZ
          i //= 3
        when 1
          ret << TP
          i //= 3
        when 2
          ret << TM
          i = (i + 1) // 3
        end
      end

      ret.map! { |j| j == TP ? TM : j == TM ? TP : TZ } if sign == -1
      ret.reverse
    end

    def to_s(a : Array(Int8), buffer = 6)
      String.build buffer do |str|
        a.map do |i|
          str << case i
          when -1 then "-"
          when  0 then "0"
          when  1 then "+"
          end
        end
      end
    end
  end

  struct Tryte
    include Comparable(Tryte)

    getter val : Int16
    def_equals_and_hash @val

    def self.valid(val : Int)
      TRYTE_RANGE.includes? val
    end

    def initialize(val : Tryte)
      initialize(val.val)
    end

    def initialize(val : Int)
      raise OverflowError.new unless Tryte.valid(val)
      @val = val.to_i16
    end

    def initialize(a : Array(Int8))
      # raise IndexError.new("Must have 6 elements, not #{a.size}") unless a.size == 6
      j = 1
      @val = 0_i16
      a.reverse.each do |i|
        @val += j * (i.clamp -1, 1)
        j *= 3
      end
    end

    def initialize(str : String)
      proc = ->(merge : Array(Int8)) { ->(j : Char) {
        case j
        when '-' then merge << TM
        when '0' then merge << TZ
        when '+' then merge << TP
        end
      } }
      str = str.rjust(2, '0')
      str = str.rjust(6, '0') if str.size > 2
      unless str.size == 2 || str.size == 6
        raise OverflowError.new "#{str} is not a valid ternary form"
      end
      a = [] of Int8
      if str.size == 2
        str.each_char do |i|
          merge = [] of Int8
          TRIBBLES.invert[i].each_char &(proc.call(merge))
          a += merge
        end
      else
        str.each_char &(proc.call(a))
      end
      initialize(a)
    end

    def trit_a
      ret = BalancedTritNotation.to_a @val
      Array(Int8).new(6 - ret.size, TZ) + ret
    end

    def trit_s
      BalancedTritNotation.to_s trit_a
    end

    def tribble_a_tuple
      a = trit_a
      {a[...3], a[3..]}
    end

    def tribble_s_tuple
      tribble_a_tuple.map do |i|
        BalancedTritNotation.to_s(i, 3)
      end
    end

    def tribble_trytes
      tribble_a_tuple.map { |i| Tryte.new([TZ, TZ, TZ] + i) }
    end

    def extr(idx)
      raise IndexError.new unless (0...6).includes? idx
      a = trit_a
      i = a[idx]
      a[idx] = TZ
      {Tryte.new(a), i}
    end

    def overflow(rel)
      t = @val + rel
      if Tryte.valid(t)
        return {0, t}
      else
        s = rel.sign
        t.abs.divmod(s * (TRYTE_MAX + 1))
      end
    end

    def succ(i = 1)
      Tryte.new(@val + i)
    end

    def <=>(other)
      @val <=> other.val
    end

    def to_s(io : IO)
      io << tribble_s_tuple.map { |i| TRIBBLES[i] }.join
    end

    def inspect(io : IO)
      io << "0t" << to_s
    end
  end

  struct TNum(N)
    include Comparable(TNum(N))
    getter trytes : StaticArray(Tryte, N)
    alias Accepts = String | Int32 | Tryte

    def_equals_and_hash @trytes

    def initialize(items : Indexable(Accepts))
      raise OverflowError.new "#{items.size} trytes can't fit inside a TNum(#{N})" if items.size > N
      items = Array(Int32).new(N - items.size, 0) + items.to_a
      @trytes = StaticArray(Tryte, N).new do |i|
        Tryte.new(items[i])
      end
    end

    def initialize(*items : Accepts)
      initialize(items)
    end

    def self.extended(item : Accepts)
      self.new Array(Accepts).new(N, item)
    end

    def initialize
      initialize([] of Tryte)
    end

    def initialize(other : TNum)
      initialize(other.trytes)
    end

    def val
      n = BigInt.new
      @trytes.each do |i|
        n *= (TRYTE_MAX + 1)
        n += i.val
      end
      n
    end

    def size
      N
    end

    def extend_with(other : TNum, offset : Int? = nil)
      offset ||= other.size
      raise OverflowError.new if offset > N
      a = @trytes.to_a
      a[-offset..] = other.trytes.to_a[-offset..]
      TNum(N).new a
    end

    def <=>(other : TNum)
      @trytes.to_a <=> (other.trytes.to_a)
    end

    def succ(i = 1)
      a = @trytes
      a.reverse_each.each_with_index do |v, j|
        i, new_val = v.overflow(i)
        a[j] = Tryte.new(new_val)
        raise OverflowError.new if i != 0 && j == (N - 1)
      end
      TNum(N).new(a.reverse!)
    end

    def inspect(io : IO)
      io << "tnum" << N.to_s << "(" << to_s << ")"
    end

    def to_s(io : IO)
      io << trytes.map(&.to_s).join(" ")
    end
  end

  class Reader
    @stream : IO
    @rem : UInt64 = 0_u8
    @rem_size : UInt8 = 0_u8

    def initialize(@stream)
    end

    def buffered_trits(amount)
      i = @rem
      i_size = @rem_size
      until i_size >= amount * 2
        i <<= 8
        i_size += 8
        k = @stream.read_byte.not_nil!
        i |= k
      end
      diff = i_size - amount * 2
      trits = [] of {b: Int32, t: Int8}
      (i_size // 2).times do
        trits << case i & 0b11
        when 0b10 then {b: 0b10, t: TM}
        when 0b00 then {b: 0b00, t: TZ}
        when 0b01 then {b: 0b01, t: TP}
        else           raise "0b11 is invalid using ETOR ternary-in-binary"
        end
        i >>= 2
      end
      trits.reverse!
      actual = trits[...amount].map &.[:t]
      @rem = trits[amount..].reduce(0_u64) do |s, t|
        (s << 2) | t[:b]
      end
      @rem_size = diff
      actual
    end

    def tryte
      Tryte.new(buffered_trits 6)
    end

    # def tribble
    #   Tryte.new([TZ, TZ, TZ] + buffered_trits 3)
    # end
  end

  # w = IOWrapper.new(IO::Memory.new(Bytes[0b00010000, 0b10000000, 0b10011000]))
  # p w.tribble.trit_s
  # p w.tribble.trit_s
  # p w.tribble.trit_s
  # p w.tribble.trit_s
end
