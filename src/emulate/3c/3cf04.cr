require "../ternary"
require "./3cml"
require "../cpu3"
require "../../etor"
require "log"

module CF04
  cpu3(3, true)

  class Reg
    Log = ::Log.for(self)
    class_getter reg_name = [
      :mmm, :mmz, :mmp, :mzm, :mzz, :mzp, :mpm, :mpz, :mpp,
      :zmm, :zmz, :zmp, :zzm, :zzz, :zzp, :zpm, :zpz, :zpp,
      :pmm, :pmz, :pmp, :pzm, :pzz, :pzp, :ppm, :ppz, :ppp,
    ]
    class_getter general_name = [
      :r01, :r02, :r03, :r04, :r05, :r06, :r07, :r08, :r09,
      :r10, :r11, :r12, :r13, :tmp, :r14, :r15, :r16, :r17,
      :spr, :bpr, :apr, :i02, :i03, :i04, :i01, :x01, :x02,
    ]
    {% begin %}
    class_getter index_tryte = [
      {% for i in (-13..13) %}
        Tr.new({{i}}.to_i16), 
      {% end %}
    ]
    property val = Array(Ternary::TNum({{REG_WIDTH}})).new(@@reg_name.size) { Ternary::TNum({{REG_WIDTH}}).new }
    {% end %}

    def self.index(t : Tr)
      (index_tryte.index t).not_nil!
    end

    def set(t : Tr, val : Addr)
      index = self.class.index t
      Log.trace &.emit "rW :: #{self.class.name_for t} <- #{val}"
      @val[index] = val
    end

    def get(t : Tr)
      index = self.class.index t
      ret = @val[index]
      Log.trace &.emit "rR :: #{self.class.name_for t} -> #{ret}"
      ret
    end

    def self.name_for(t : Tr, reg = true, general = false, extended = false)
      reg = false if general
      reg, general = true, true if extended
      index = self.index t
      String.build do |str|
        str << "@" << Reg.reg_name[index].to_s if reg
        str << "/" if reg && general
        str << "@" << Reg.general_name[index].to_s if general && !reg
        str << Reg.general_name[index].to_s if extended
      end
    end
  end

  class Sim
    getter files = Hash(Addr, Array(Tr)).new() { |hash, key|
      hash[key] = ([] of Tr)
    }
    property sra = Addr.new
    getter privilege = 0_u8

    def execute_at(addr : Addr)
      memread.stream.position = addr
      running = true

      while running
        @sra = memread.stream.position
        Log.info &.indented "Executing from #{memread.stream.position}"
        # raise Violation::SegmentationFault.new @sra, Permission.x unless containing_segment(@sra).executable?
        i = memread.tryte x: true
        i, size = i.extr(4)
        i, dir = i.extr(5)
        Log.trace &.emit "Executing #{i}"
        ret = ISA3::INSTR_TABLE[i]?.try &.handle(self, size, dir) || raise Violation::IllegalOperation.new i
        if ret.halt?
          Log.warn &.end "Execution has gracefully halted"
          running = false
        elsif ret.unknown_syscall?
          memread.stream.seek -1
          raise Violation::InvalidSyscall.new memread.tryte
        else
          Log.info &.end "Next instruction..."
        end
      end
    end
  end

  module Violation
    class IllegalOperation < ViolationException
      def initialize(instruction : Tr)
        super("Illegal instruction")
        instruction_trit = instruction.trit_s[...4]
        @diagnostic = "Tried executing #{instruction} (#{instruction_trit}), but there's no such entry in the INSTR_TABLE"
      end
    end

    class InvalidSyscall < ViolationException
      def initialize(instruction : Tr)
        super("Invalid system call")
        @diagnostic = "Tried yielding to syscall #{instruction}, but there's no such system call defined"
      end
    end
  end

  s = Sim.new
  s.memread.write ~"LM", "HG mJ mg Hh dI 0J Hk mH 0A iI 0B iI 00 LI IJ LD LI ac 00"
  s.segments << {(~"LM"...~"Ma"), Sim::Permission.rx}
  s.segments << {(~"Ma"...~"ME"), Sim::Permission.r}

  s.hypervisor_execute ~"LM"

  puts "Output from file A: \n"
  puts "--------\n" + (TernaryAscii.convert s.files[~"A"]) + "\n--------"
  puts (TernaryAscii.convert s.files[~"A"]).bytes.to_s
end
