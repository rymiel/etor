require "../ternary"
require "./3cf04"

module ISA3
  INSTR_TABLE = {} of Tr => Instr

  macro instruction(name, code, *flags, &proc)
    {% code = code + "00" %}
    INSTR_TABLE[Tr.new({{code}})] = Instr.new(
      {{name.id.stringify}}, Fl.new({{*flags}})
    ) do |ctx, log| {{proc.body}} end
  end

  struct Fl
    FIELDS = [] of {Symbol, String, Int32}

    macro field(name, parent, pos = nil)
      {% unless pos %}
        {% if FIELDS.last && FIELDS.last[1] == parent.id.stringify %}
          {% pos = FIELDS.last[2] + 1 %}
        {% end %}
      {% end %}
      {% pos = 1 unless pos %}
      {% FIELDS.push({name, parent.id.stringify, pos}) %}
      def {{name.id}}?
        @{{parent.id}} & (1 << {{pos}}) > 0
      end
    end

    def initialize(*syms : Symbol)
      {% for i in FIELDS %}
      @{{i[1].id}} |= (1 << {{i[2]}}) if syms.includes?({{i[0]}})
      {% end %}
    end

    def initialize(copy : Fl)
      self | copy
    end

    def ^(other : Fl)
      {% for i in @type.instance_vars %}
      @{{i.name}} ^= other.{{i.name}}
      {% end %}
      self
    end

    def |(other : Fl)
      {% for i in @type.instance_vars %}
      @{{i.name}} |= other.{{i.name}}
      {% end %}
      self
    end

    def |(other : Symbol)
      self | Fl.new(other)
    end

    def ^(other : Symbol)
      self ^ Fl.new(other)
    end

    property meta : Int8 = 0_i8
    property binary : Int32 = 0_i32

    field :op_no, :meta   # Takes no operands
    field :op_un, :meta   # Takes a single operand (unary)
    field :op_bi, :meta   # Takes two operands (binary)
    field :no_auto, :meta # Don't do automatic conversions

    field :bi_dst_reg, :binary # The destination may be a register
    field :bi_dst_mem, :binary # The destination may be a memory location
    field :bi_src_lit, :binary # The source may be a literal
    field :bi_src_loc, :binary # The source may be a location (reg + mem)
  end

  alias Tn = Ternary::TNum
  alias Tr = Ternary::Tryte
  alias Tb = Int8

  struct Context
    getter size : Size
    getter dir : Direction
    property! raw : {aux: Tr, r_arg: Tr, r: Tuple(Tr)}?
    property! src : DataSource?
    property! dst : DataSource?
    property! direct : CF04::Sim?

    def initialize(@size, @dir, @raw = nil)
    end
  end

  enum Size : Tb
    Tryte = -1_i8
    Word
    Long
  end

  enum Direction : Tb
    RegAux = -1_i8
    RegReg
    AuxReg
  end

  module DataClassifications
    enum Type
      Reg
      Const
      Mem
      MemPtr
      MemDis1
      MemDis3
      MemDis9
      RegSra
      RegDisR
      RegDisH
      RegDis1
      RegDis3
      RegDis9
      SraDis1
      SraDis3
      SraDis9
      Sra
    end

    @type : Type

    def unassignable?
      @type.sra? || @type.const? || @type.reg?
    end

    def literal?
      @type.const? || @type.sra?
    end

    def location?
      !literal?
    end

    def register?
      @type.reg?
    end

    def memory?
      location? && !@type.reg?
    end

    def no_reads?
      @type.sra? || @type.reg?
    end
  end

  class DataSource
    include DataClassifications
    Log = ::Log.for(self)

    getter! data : CF04::Addr?
    getter ef_addr : CF04::Addr?
    getter ef_size : Int32
    getter type : DataClassifications::Type
    getter reads : Int32 = 0
    @s : CF04::Sim?
    @reg_name : Tr?

    def initialize(type : Tr, i_size i, w_size w)
      @ef_size = i
      @type, @reads =
        case type
        when "---" then {Type::Const, i}
        when "--0" then {Type::Mem, w}
        when "--+" then {Type::MemPtr, w}
        when "-+-" then {Type::MemDis1, w + 1}
        when "-+0" then {Type::MemDis3, w + 3}
        when "-++" then {Type::MemDis9, w + 9}
        when "000" then {Type::RegSra, 1}
        when "+00" then {Type::RegDisH, 1}
        when "+0+" then {Type::RegDis1, 2}
        when "++0" then {Type::RegDis3, 4}
        when "+++" then {Type::RegDis9, 10}
        when "0++" then {Type::RegDisR, 1}
        when "0--" then {Type::SraDis1, 1}
        when "0-0" then {Type::SraDis3, 3}
        when "0-+" then {Type::SraDis9, 9}
        when "0+0" then {Type::Sra, 0}
        else
          raise "Invalid addressing mode #{type}"
        end
    end

    def initialize(reg : Tr)
      @type = Type::Reg
      @reg_name = reg
      @ef_size = CF04::REG_WIDTH
    end

    alias Loc = Array(Tr) | CF04::Addr

    def _loc(a : Loc)
      CF04::Addr.new a
    end

    def _ptr(a : Loc, size : Int32? = nil)
      Log.trace &.emit "Resolving pointer #{a} - size #{size} using #{@s}"
      _loc @s.not_nil!.memread.read(_loc(a), size || CF04::REG_WIDTH)
    end

    def _reg(a : Tr)
      @s.not_nil!.reg.val[@s.not_nil!.reg.class.index_tryte.index(a).not_nil!]
    end

    def fetch(s : CF04::Sim, adr_data : Array(Tr))
      if !adr_data.empty? && no_reads?
        raise ContextBuilderError.new "Non-adr data source cannot read additional memory, however #{@type} was provided with #{adr_data}"
      end
      @s = s
      ef_ptr = ->(a : Loc) { {_ptr(a, @ef_size), _loc(a)} }
      ef_lit = ->(a : CF04::Addr) { {a, nil} }
      @data, @ef_addr =
        case @type
        in .reg?      then ef_lit.call _reg @reg_name.not_nil!
        in .sra?      then ef_lit.call s.sra
        in .const?    then ef_lit.call _loc adr_data
        in .mem?      then ef_ptr.call adr_data
        in .mem_ptr?  then ef_ptr.call _ptr adr_data
        in .mem_dis1? then ef_ptr.call (_ptr adr_data[...-1]).succ adr_data[-1].val
        in .mem_dis3? then ef_ptr.call (_ptr adr_data[...-3]).succ Tn(3).new(adr_data[-3..-1]).val
        in .mem_dis9? then ef_ptr.call (_ptr adr_data[...-9]).succ Tn(9).new(adr_data[-9..-1]).val
        in .reg_sra?  then ef_lit.call s.sra.succ (_reg adr_data[0].tribble_trytes[0]).val
        in .reg_dis1? then ef_ptr.call (_reg adr_data[0].tribble_trytes[0]).succ adr_data[1].val
        in .reg_dis3? then ef_ptr.call (_reg adr_data[0].tribble_trytes[0]).succ Tn(3).new(adr_data[1..3]).val
        in .reg_dis9? then ef_ptr.call (_reg adr_data[0].tribble_trytes[0]).succ Tn(9).new(adr_data[1..9]).val
        in .sra_dis1? then ef_lit.call s.sra.succ adr_data[0].val
        in .sra_dis3? then ef_lit.call s.sra.succ Tn(3).new(adr_data[0...3]).val
        in .sra_dis9? then ef_lit.call s.sra.succ Tn(9).new(adr_data[0...9]).val
        in .reg_dis_r?
          orig_reg, disp_reg = adr_data[0].tribble_trytes
          ef_ptr.call (_reg orig_reg).succ (_reg disp_reg).val
        in .reg_dis_h?
          orig_reg, disp = adr_data[0].tribble_trytes
          ef_ptr.call (_reg orig_reg).succ disp.val
        end
      Log.debug &.emit "#{@type} addressing resolved to #{@data} - #{@ef_size} trytes " + (@ef_addr ? "(located at #{@ef_addr})" : "(not in memory)")
    end

    def write_to(new_data : CF04::Addr, new_ef_size : Int32 = CF04::REG_WIDTH)
      raise DataSourceAbstractionError.new if literal?
      if @type.reg?
        reg = @reg_name.not_nil!
        Log.debug &.indented "Writing #{new_data} - #{new_ef_size} trytes to register #{CF04::Reg.name_for reg, extended: true}"
        merged = @data.not_nil!.extend_with new_data, new_ef_size
        Log.will_dedent
        @s.not_nil!.reg.set reg, (CF04::Addr.new merged)
      end
    end

    def write_to(new_data : DataSource)
      write_to new_data.data, new_data.ef_size
    end
  end

  class ContextBuilderError < Exception
  end

  class DataSourceAbstractionError < Exception
  end

  enum SysState
    Ok
    Halt           = 1040
    UnknownSyscall =  880
  end

  class Instr
    getter name : String
    getter flags : Fl
    Log = ::Log.for(self)

    def initialize(@name, @flags, &@proc : Context, ::Log -> SysState)
    end

    def handle(s : CF04::Sim, size : Tb, dir : Tb)
      size = Size.new(size)
      dir = Direction.new(dir)
      ctx = Context.new(size, dir)

      src : DataSource?
      dst : DataSource?

      if @flags.no_auto?
        Log.trace &.emit "Passing sim to instruction directly"
        ctx.direct = s
      elsif @flags.op_bi?
        unless dir.reg_reg?
          adr = s.memread.tribble x: true
          reg = s.memread.tribble x: true
          instr_size = case size
                       in .tryte? then 1
                       in .word?  then 3
                       in .long?  then 9
                       end
          word_size = CF04::REG_WIDTH
          adr = DataSource.new(adr, instr_size, word_size)
          Log.debug &.indented "#{adr.type} addressing mode will read #{adr.reads} more trytes"
          value_trytes = Array(Tr).new(adr.reads) { s.memread.tryte x: true }
          Log.debug &.end
          adr.fetch(s, value_trytes)
          reg = DataSource.new(reg)
          reg.fetch(s, [] of Tr)
          if dir.reg_aux? && adr.unassignable?
            raise ContextBuilderError.new "Unassignable destination"
          elsif dir.reg_aux?
            src = reg
            dst = adr
          else
            src = adr
            dst = reg
          end
        else
          src = DataSource.new(s.memread.tribble x: true)
          dst = DataSource.new(s.memread.tribble x: true)
        end
        err = [] of String
        err << "Source may not be literal" if !@flags.bi_src_lit? && src.literal?
        err << "Source may not be a location" if !@flags.bi_src_loc? && src.location?
        err << "Destination may not be a memory address" if !@flags.bi_dst_mem? && dst.memory?
        err << "Destination may not be a register" if !@flags.bi_dst_reg? && dst.register?
        unless err.empty?
          raise ContextBuilderError.new "#{name} is incompatible with data because: #{err.join('\n')}"
        end
        ctx.src = src
        ctx.dst = dst
      elsif @flags.op_no?
        Log.trace &.emit "Nothing to read"
      end

      call(ctx)
    end

    def call(ctx)
      log = Log.for(@name)
      log.info &.indented "Passing context to handler for #{@name}"
      ret = @proc.call(ctx, log)
      log.info &.end "Handler exited with system state #{ret}"
      ret
    end
  end

  STD_BI_OP = Fl.new(:op_bi, :bi_dst_reg, :bi_dst_mem, :bi_src_lit, :bi_src_loc)
  STD_NO_OP = Fl.new(:op_no)

  instruction :MOV, "+0--", (STD_BI_OP) {
    ctx.dst.write_to ctx.src
    SysState::Ok
  }

  instruction :MOVUB, "+0-+", (STD_BI_OP ^ :bi_dst_mem) {
    ext = (CF04::Addr.extended "mm").extend_with ctx.src.data, ctx.src.ef_size
    ctx.dst.write_to ext
    SysState::Ok
  }

  instruction :ADD, "++++", (STD_BI_OP) {
    ext = ctx.dst.data.succ ctx.src.data.val
    ctx.dst.write_to ext
    SysState::Ok
  }

  instruction :SYS, "-00+", (Fl.new :no_auto, :op_un) {
    syscall = ctx.direct.memread.tryte x: true
    log.info &.emit "Executing syscall #{syscall}"
    case syscall
    when "00" then next SysState::Halt
    when "0B"
      pointer = ctx.direct.reg.get Tr.new("+00")
      file = ctx.direct.reg.get Tr.new("+0-")
      size = (ctx.direct.reg.get Tr.new("+0+")).val + (CF04::Addr.extended "MM").val
      temp_out = ::Log.for("SYSCALL_OUT")
      data = ctx.direct.memread.read pointer, size
      temp_out.info &.emit "#{file} => #{data}"
      ctx.direct.files[file] += data
    else
      SysState::UnknownSyscall
    end
    SysState::Ok
  }

  instruction :HLT, "-00-", (STD_NO_OP) {
    SysState::Halt
  }
end
