require "../ternary"
require "../cpu3"
require "./322dml"
require "../../etor"
require "log"
require "crsfml"
require "sdl"

module C22D
  cpu3(2, false)

  class Reg
    Log = ::Log.for(self)
    class_getter reg_name = [
      :m, :l, :k, :j, :o, :a, :b, :c, :d,
    ]
    {% begin %}
    class_getter index_tryte = [
      {% for i in (-4..4) %}
        Tr.new({{i}}.to_i16), 
      {% end %}
    ]
    property val = Array(Ternary::TNum({{REG_WIDTH}})).new(@@reg_name.size) { Ternary::TNum({{REG_WIDTH}}).new }
    {% end %}

    def self.index(t : Tr)
      (index_tryte.index t).not_nil!
    end

    def set(t : Tr, val : Addr)
      index = self.class.index t
      Log.trace &.emit "rW :: #{self.class.name_for t} <- #{val}"
      @val[index] = val
    end

    def get(t : Tr)
      index = self.class.index t
      ret = @val[index]
      Log.trace &.emit "rR :: #{self.class.name_for t} -> #{ret}"
      ret
    end

    def self.name_for(t : Tr)
      index = self.index t
      "@" + Reg.reg_name[index].to_s
    end
  end

  class Sim
    property ip = Addr.new

    def execute_at(addr : Addr)
      memread.stream.position = addr
      running = true

      while running
        sleep 0
        @ip = memread.stream.position
        Log.info &.indented "Executing from #{memread.stream.position}"
        raise Violation::AlignmentFault.new @ip unless @ip.val % 2 == 0
        upper = memread.tryte
        lower = memread.tryte
        Log.trace &.emit "Executing #{upper} #{lower}"
        ret = ISA3.handle(lower, upper) || raise Violation::IllegalOperation.new lower, upper
        if ret.halt?
          Log.warn &.end "Execution has gracefully halted"
          running = false
        else
          Log.info &.end "Next instruction..."
        end
      end
    end
  end

  module Violation
    class AlignmentFault < ViolationException
      def initialize(location : Addr)
        super("Alignment fault")
        @diagnostic = "Memory location #{location} isn't word-aligned (#{location.val})"
      end
    end

    class IllegalOperation < ViolationException
      def initialize(lower : Tr, upper : Tr)
        super("Illegal instruction")
        @diagnostic = "Tried executing #{upper} #{lower}, but failed"
      end
    end
  end

  s = Sim.new
  s.memread.repeated_write ~"00", [Tr.new("MI"), Tr.new("00")], 6000
  s.memread.write ~"AE KE", "MI mm"

  spawn do
    s.hypervisor_execute ~"AE KA"
    puts "exiting CPU fiber.."
  end

  FRAME_LIMIT = 1 / 30
  W           = 160
  H           = 144

  SDL.init(SDL::Init::VIDEO)

  window = SDL::Window.new("SDL test", W, H)
  renderer = SDL::Renderer.new(window, SDL::Renderer::Flags::ACCELERATED)
  pixel_format = ((1 << 28) | (6 << 24) | (1 << 20) | (6 << 16) | (24 << 8) | (4 << 0)).to_u32
  texture = LibSDL.create_texture(renderer, pixel_format, LibSDL::TextureAccess::STATIC, W, H)
  framebuffer = Array(UInt32).new(W * H) { 0_u32 }

  i = 0

  clock = Time.monotonic
  ftbuffer = [] of Float32

  loop do
    while event = SDL::Event.poll
      case event
      when SDL::Event::Quit
        LibSDL.destroy_texture texture
        SDL.quit
        ftbuffer.each { |f| puts (
          "#{(f * 1000_000).to_i.to_s.rjust 6}us / " \
          "#{(FRAME_LIMIT * 1000_000).to_i.to_s.rjust 6}us (" \
          "#{((FRAME_LIMIT - f) * 1000_000).to_i.to_s.rjust 6}us spare, " \
          "#{(f / FRAME_LIMIT * 100).to_i.to_s.rjust 3}%)"
        ) }
        exit
      end
    end

    ft_start = Time.monotonic
    i += (ft_start - clock).microseconds // 9600
    r = (i % (255 * 2) - 255).abs.to_i
    clock = ft_start

    (0...H).each do |y|
      (0...W).each do |x|
        color_g = (255 * (x / W)).to_u32
        color_b = (255 * (y / H)).to_u32
        framebuffer[y * W + x] = 0x00FFFFFF_u32 & ((r << 16) + (color_g << 8) + color_b)
      end
    end

    LibSDL.update_texture texture, Pointer(SDL::Rect).null, framebuffer, W * 4

    renderer.clear
    renderer.copy texture
    renderer.present
    ft_end = Time.monotonic
    ft = (ft_end - ft_start).total_seconds.to_f32
    ftbuffer << ft
    sleep (FRAME_LIMIT - ft).clamp(0, FRAME_LIMIT) unless ft > FRAME_LIMIT
  end
end
