require "../ternary"
require "./3c22d"

module ISA3
  alias Tn = Ternary::TNum
  alias Tr = Ternary::Tryte
  alias Tb = Int8
  Log = ::Log.for(self)

  enum Encoding
    TriadicImmediate
    TriadicRegister
    DiadicImmediate
    DiadicRegister
    LdstImmediate
    LdstRegister
    LdstPattern
    BranchConditional
    BranchDirect
    JumpConditional
    JumpDirect
  end

  ENCODING_MNEMONIC = {
    :TI => Encoding::TriadicImmediate,
    :TR => Encoding::TriadicRegister,
    :DI => Encoding::DiadicImmediate,
    :DR => Encoding::DiadicRegister,
    :RI => Encoding::LdstImmediate,
    :RR => Encoding::LdstRegister,
    :RP => Encoding::LdstPattern,
    :BC => Encoding::BranchConditional,
    :BD => Encoding::BranchDirect,
    :JC => Encoding::JumpConditional,
    :JD => Encoding::JumpDirect,
  }

  INSTR_TABLE = {} of Encoding => Hash(Tr, Instr)
  {% for i in Encoding.constants %}
  INSTR_TABLE[Encoding::{{i}}] = {} of Tr => Instr
  {% end %}

  macro instruction(name, encoding, subopcode, &proc)
    INSTR_TABLE[{{ ENCODING_MNEMONIC[encoding] }}][Tr.new {{ subopcode }}] = Instr.new(
      {{name.id.stringify}}, {{ ENCODING_MNEMONIC[encoding] }}
    ) do |ctx, log| {{proc.body}} end
  end

  class Instr
    getter name : String
    getter encoding : Encoding
    Log = ::Log.for(self)

    def initialize(@name, @encoding, &@proc : Array(Tr), ::Log -> SysState)
    end

    def call(ctx)
      log = Log.for(@name)
      log.info &.indented "Passing context #{ctx} to handler for #{@name} (#{@encoding})"
      ret = @proc.call(ctx, log)
      log.info &.end "Handler exited with system state #{ret}"
      ret
    end
  end

  private def self._reg(tr)
    C22D::Reg.name_for tr
  end

  def self.handle(lower : Tr, upper : Tr)
    log = Log.for("handle")
    soc, form, args =
      if upper < Tr.new "Em" # mmmm:DMMM - Triadic Immediate
        log.debug &.indented "TI (iIDDSS ZZZZZZ)"
        log.debug &.emit "   (#{upper.trit_s} #{lower.trit_s})"
        subopcode = Tr.new upper.trit_a[0..1]
        dest = Tr.new upper.trit_a[2..3]
        source_reg = Tr.new upper.trit_a[4..5]
        source_imm = Tr.new lower
        log.debug &.end "i:#{subopcode} d:#{_reg dest} s:#{_reg source_reg} z:#{source_imm}"
        {subopcode, Encoding::TriadicImmediate, [dest, source_reg, source_imm]}
      elsif upper < Tr.new "Gm" # Emmm:FMMM - Load/Store Immediate
        log.debug &.indented "RI (+-iIDD SSSSSS)"
        log.debug &.emit "   (#{upper.trit_s} #{lower.trit_s})"
        subopcode = Tr.new upper.trit_a[2..3]
        dest = Tr.new upper.trit_a[4..5]
        source = Tr.new lower
        log.debug &.end "i:#{subopcode} d:#{_reg dest} s:#{source}"
        {subopcode, Encoding::LdstImmediate, [dest, source]}
      elsif upper < Tr.new "GE"
        if lower < Tr.new "Em" # G(mD)(mD)* - Load/Store Pattern
          log.debug &.indented "RP (+-+pDD iIPSSS)"
          log.debug &.emit "   (#{upper.trit_s} #{lower.trit_s})"
          subopcode = Tr.new lower.trit_a[0..1]
          dest = Tr.new upper.trit_a[4..5]
          source = Tr.new lower.trit_a[3..5]
          pattern = Tr.new (upper.trit_a[3..3] + lower.trit_a[2..2])
          log.debug &.end "i:#{subopcode} d:#{_reg dest} s:#{source} p:#{pattern}"
          {subopcode, Encoding::LdstPattern, [dest, source, pattern]}
        else # G(mD)(EM)* - Diadic Immediate
          log.debug &.indented "DI (+-+iRR +ISSSS)"
          log.debug &.emit "   (#{upper.trit_s} #{lower.trit_s})"
          subopcode = Tr.new (upper.trit_a[3..3] + lower.trit_a[1..1])
          reg = Tr.new upper.trit_a[4..5]
          source = Tr.new lower.trit_a[2..5]
          log.debug &.end "i:#{subopcode} r:#{_reg reg} s:#{source}"
          {subopcode, Encoding::DiadicImmediate, [reg, source]}
        end
      elsif upper < Tr.new "Hm" # GEmm:GMMM - Load/Store Register
        log.debug &.indented "RR (+-++DD IISSZZ)"
        log.debug &.emit "   (#{upper.trit_s} #{lower.trit_s})"
        subopcode = Tr.new lower.trit_a[0..1]
        dest = Tr.new upper.trit_a[4..5]
        base = Tr.new lower.trit_a[2..3]
        offset = Tr.new lower.trit_a[4..5]
        log.debug &.end "i:#{subopcode} d:#{_reg dest} s:#{_reg base} z:#{_reg offset}"
        {subopcode, Encoding::LdstRegister, [dest, base, offset]}
      elsif upper < Tr.new "Km" # Hmmm:JMMM - Branch Conditional
        log.debug &.indented "BC (+0CCRR SSSSSS)"
        log.debug &.emit "   (#{upper.trit_s} #{lower.trit_s})"
        condition = Tr.new upper.trit_a[2..3]
        operand = Tr.new upper.trit_a[4..5]
        offset = Tr.new lower
        log.debug &.end "c:#{condition} r:#{_reg operand} s:#{offset}"
        {(Tr.new 0), Encoding::BranchConditional, [condition, operand, offset]}
      elsif upper < Tr.new "Mm" # Kmmm:LMMM - Branch Direct
        log.debug &.indented "BD (++mSSS SSSSSS)"
        log.debug &.emit "   (#{upper.trit_s} #{lower.trit_s})"
        mode = Tr.new upper.trit_a[2..2]
        offset_u = Tr.new upper.trit_a[4..5]
        offset_l = Tr.new lower
        log.debug &.end "m:#{mode} s:#{offset_u} #{offset_l}"
        {(Tr.new 0), Encoding::BranchDirect, [mode, offset_u, offset_l]}
      else
        if lower < Tr.new "Em" # M(EM)(mD)* - Diadic Register
          log.debug &.indented "DR (++++DD iIIISS)"
          log.debug &.emit "   (#{upper.trit_s} #{lower.trit_s})"
          subopcode = Tr.new lower.trit_a[0..3]
          dest = Tr.new upper.trit_a[4..5]
          source = Tr.new lower.trit_a[4..5]
          log.debug &.end "i:#{subopcode} d:#{_reg dest} s:#{source}"
          {subopcode, Encoding::DiadicRegister, [dest, source]}
        elsif lower < Tr.new "Jm" # M(EM)(EJ)* - Jump Direct
          log.debug &.indented "JD (++++SS +mRRZZ)"
          log.debug &.emit "   (#{upper.trit_s} #{lower.trit_s})"
          mode = Tr.new lower.trit_a[1..1]
          base = Tr.new upper.trit_a[4..5]
          operand = Tr.new lower.trit_a[2..3]
          offset = Tr.new lower.trit_a[4..5]
          log.debug &.end "m:#{mode} r:#{_reg operand} s:#{_reg base} z:#{_reg offset}"
          {(Tr.new 0), Encoding::JumpDirect, [mode, operand, base, offset]}
        else # M(EM)(JM)* - Jump Conditional
          log.debug &.indented "JD (++++RR ++CCSS)"
          log.debug &.emit "   (#{upper.trit_s} #{lower.trit_s})"
          operand = Tr.new upper.trit_a[4..5]
          condition = Tr.new lower.trit_a[2..3]
          offset = Tr.new lower.trit_a[4..5]
          log.debug &.end "c:#{condition} r:#{_reg operand} s:#{_reg offset}"
          {(Tr.new 0), Encoding::JumpConditional, [condition, operand, offset]}
        end
      end

    INSTR_TABLE[form][soc]?.try &.call args
  end

  enum SysState
    Ok
    Halt = 1040
  end

  instruction :HLT, :DR, "----" {
    SysState::Halt
  }

  instruction :NOP, :DR, "0000" {
    SysState::Ok
  }
end
