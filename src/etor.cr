require "colorize"
require "log"

def shorten(string : String, to : Int)
  string = string.rjust(to)
  string = "…" + string[-to + 1..] if string.size > to
  string
end

# TODO: Write documentation for `Etor`
module Etor
  VERSION = "0.1.0"

  LOGGER_COLORS = {
    ::Log::Severity::Fatal => :magenta,
    ::Log::Severity::Error => :red,
    ::Log::Severity::Warn  => :yellow,
    ::Log::Severity::Info  => :blue,
    ::Log::Severity::Debug => :cyan,
    ::Log::Severity::Trace => :dark_gray,
  }

  Log = Log.for(".")
  ::Log.setup(:info, ::Log::IOBackend.new(formatter: Etor::Formatter, dispatcher: ::Log::DispatchMode::Async))

  struct Formatter < ::Log::StaticFormatter
    @@indents = 0
    @@source_width = 18
    @@indent_expectation = 5
    @@prelude : Int32 = 4 + # [] and spaces
      @@source_width +
      @@indent_expectation

    def run
      will_indent = @entry.data[:indents].as_bool
      will_dedent = @entry.data[:dedents].as_bool
      will_reset = @entry.data[:resets].as_bool

      source = @entry.source
      source = "." if source.empty?
      source = shorten(source, @@source_width)
      source = "[#{source}] "

      color = LOGGER_COLORS[@entry.severity]? || :white

      @io << source.colorize(color).bold

      indent_adjust = 0
      if will_reset
        @io << "\u2534" * @@indents
        indent_adjust, @@indents = @@indents, 0
        will_indent, will_dedent = false, false
      end
      @@indents += 1 if will_indent
      @@indents.times do |i|
        if i == @@indents - 1
          @io << (will_indent ? "\u250c" : (will_dedent ? "\u2514" : "\u2502"))
        else
          @io << "\u2502"
        end
      end
      @io << ((will_indent || will_dedent) ? "\u2500" : " ") * (@@indent_expectation - @@indents - indent_adjust).clamp(0..)
      # @io << (will_reset ? "r" : " ")
      # @io << (will_indent ? ">" : " ")
      # @io << (will_dedent ? "<" : " ")
      @io << " "
      @@indents -= 1 if will_dedent

      severe = @entry.severity.fatal? || @entry.severity.error?

      @io << @entry.message.gsub("\n", "\n#{" " * @@prelude}").colorize(severe ? color : :white)
    end
  end
end

class Log
  struct Emitter
    class_setter will_indent = false
    class_setter will_dedent = false
    class_setter will_reset = false

    def indented(message : String = "", **kwargs)
      emit(message, kwargs.merge(indents: true, dedents: false))
    end

    def end(message : String = "", **kwargs)
      emit(message, kwargs.merge(dedents: true, indents: false))
    end

    def emit(message : String, data : Metadata | Hash | NamedTuple)
      indents = @@will_indent || (data[:indents]?.coa false, &.== true)
      dedents = @@will_dedent || (data[:dedents]?.coa false, &.== true)
      resets = @@will_reset
      @@will_dedent = false
      @@will_indent = false
      @@will_reset = false
      previous_def(message, data.to_h.merge({
        :dedents => dedents,
        :indents => indents,
        :resets  => resets,
      }))
    end
  end

  def will_indent
    Emitter.will_indent = true
  end

  def will_dedent
    Emitter.will_dedent = true
  end

  def dedent_all
    Emitter.will_reset = true
  end
end

# :nodoc:
class Object
  # :nodoc:
  def coa(s : T) forall T
    self.nil? ? s : yield self
  end
end
